<?php

use App\Entities\Buyer;
use App\Entities\ShipmentOrder;
use PHPUnit\Framework\TestCase;

final class OrderServiceTest extends TestCase
{
    public function testShip(){
        $order = new ShipmentOrder(16400);
        $order->load();

        $buyer = new Buyer(29664);
        $buyer->load();

        $ordersService = new \App\Services\OrderService();

        $exptectedTrackingNumber = "";
        $trackingNumber = $ordersService->ship($order, $buyer);

        $this->assertTrue($trackingNumber == $exptectedTrackingNumber);
    }
}