<?php
namespace App\Utils;

class FileSystem
{
    public static function loadFile(string $path) : string|bool {
        if(file_exists($path)){
            return file_get_contents($path);
        }
        return false;
    }
}