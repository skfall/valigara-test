<?php

namespace App\Utils;

class Logger
{
    const FILE = __DIR__ . '/../../logs/log.txt';

    public static function log(string $message) : void {
        $date = (new \DateTime())->format('Y-m-d H:i:s');

        $fp = fopen(self::FILE, 'a');
        fwrite($fp, "[$date] $message\n");
        fclose($fp);
    }
}