<?php
namespace App;

use App\Entities\Buyer;
use App\Entities\ShipmentOrder;
use App\Managers\OrdersManager;

class App
{
    public function start() : void {
        $order = new ShipmentOrder(16400);
        $order->load();

        $buyer = new Buyer(29664);
        $buyer->load();

        $ordersManager = new OrdersManager($order, $buyer);

        $trackingNumber = $ordersManager->processOrder();
        echo $trackingNumber ?: "Failed to obtain tracking number. Please, look at [logs/log.txt] file";
    }
}