<?php

namespace App\Services;

use App\Api\FbaApi;
use App\Entities\AbstractOrder;
use App\Entities\BuyerInterface;

class OrderService implements ShippingServiceInterface
{
    private FbaApi $fbaApi;

    /**
     *
     */
    public function __construct(){
        $this->fbaApi = new FbaApi();
    }

    /**
     * @param AbstractOrder $order
     * @param BuyerInterface $buyer
     * @return string
     */
    public function ship(AbstractOrder $order, BuyerInterface $buyer): string
    {
        try {
            return $this->fbaApi->shipOrder($order, $buyer);
        } catch (\Throwable $th){
            throw new \RuntimeException('Service is unavailable');
        }
    }
}