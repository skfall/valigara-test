<?php

namespace App\Managers;

use App\Entities\AbstractOrder;
use App\Entities\BuyerInterface;
use App\Services\OrderService;
use App\Utils\Logger;

class OrdersManager
{
    private string $response = "";
    private OrderService $orderService;

    /**
     * @param AbstractOrder $order
     * @param BuyerInterface $buyer
     */
    public function __construct(
        private AbstractOrder $order,
        private BuyerInterface $buyer
    ){
        $this->orderService = new OrderService();
    }

    /**
     * @return string
     */
    public function processOrder() : string {
        try {
            $this->response = $this->orderService->ship($this->order, $this->buyer);
        } catch (\RuntimeException $exception){
            Logger::log($exception->getMessage());
        }

        return $this->response;
    }
}