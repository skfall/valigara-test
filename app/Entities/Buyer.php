<?php

namespace App\Entities;

use App\Utils\FileSystem;

class Buyer implements BuyerInterface
{

    private int $id;
    private array $buyer = [];

    public function __construct(int $id){
        $this->id = $id;
        $this->loadData($id);
    }

    public function getBuyerId() : int {
        return $this->id;
    }

    public function load() : void {
        $this->loadData($this->getBuyerId());
    }

    public function loadData(int $id) : void {
        $path = __DIR__ . "/../Mock/buyer.$id.json";
        $data = FileSystem::loadFile($path);
        $this->buyer = $data ? json_decode($data, true) : [];
    }

    public function offsetExists($offset) : bool
    {
        return isset($this->buyer[$offset]);
    }

    public function offsetGet($offset) : mixed
    {
        return $this->buyer[$offset] ?? null;
    }

    public function offsetSet($offset, $value) : void
    {
        if (is_null($offset)) {
            $this->buyer[] = $value;
        } else {
            $this->buyer[$offset] = $value;
        }
    }

    public function offsetUnset($offset) : void
    {
        unset($this->buyer[$offset]);
    }
}