<?php

namespace App\Entities;

use App\Utils\FileSystem;

class ShipmentOrder extends AbstractOrder
{

    protected function loadOrderData(int $id): array
    {
        $path = __DIR__ . "/../Mock/order.$id.json";
        $data = FileSystem::loadFile($path);
        return $data ? json_decode($data, true) : [];
    }
}