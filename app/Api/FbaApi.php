<?php
namespace App\Api;

use App\Entities\AbstractOrder;
use App\Entities\BuyerInterface;
use App\Utils\Logger;
use GuzzleHttp\Client;

class FbaApi
{
    private Client $httpClient;
    private string $host;
    private ?string $marketplaceId;
    private ?string $developerId;
    private ?string $sellingPartnerId;
    private ?string $mwsAuthToken;
    private ?string $accessToken = null;

    public function __construct(){
        $this->httpClient = new Client(['verify' => false]);
        $this->host = "https://sellingpartnerapi-na.amazon.com/";

        $this->developerId = env('AWS_DEVELOPER_ID');
        $this->sellingPartnerId = env('AWS_SELLING_PARTNER_ID');
        $this->mwsAuthToken = env('AWS_MWS_AUTH_TOKEN');
        $this->marketplaceId = env('AWS_MARKETPLACE_ID');

        $this->auth();
    }

    private function auth() : void {
        $endpoint = $this->host.'authorization/v1/authorizationCode';

        $options = [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
            'query' => [
                'sellingPartnerId' => $this->sellingPartnerId,
                'developerId' => $this->developerId,
                'mwsAuthToken' => $this->mwsAuthToken
            ]
        ];

        try {
            $result = $this->call($endpoint, $options);

            $accessToken = $result['data']['payload']['authorizationCode'] ?? null;

            if($accessToken !== null){
                $this->accessToken = $accessToken;
            }
        } catch (\Throwable $th){
            Logger::log('Auth Error: '.$th->getMessage());
        }
    }

    /**
     * @param string $endpoint
     * @param array $options
     * @param string $method
     * @return array
     */
    private function call(string $endpoint, array $options, string $method = 'GET') : array {
        try {
            $response = $this->httpClient->request($method, $endpoint, $options);
            return [
                'data' => json_decode($response->getBody()->getContents(), true),
                'statusCode' => $response->getStatusCode()
            ];
        }catch (\Throwable $th){
            Logger::log('Api Call Error: '.$th->getMessage());
            throw new \RuntimeException($th->getMessage());
        }
    }

    /**
     * @param AbstractOrder $order
     * @param BuyerInterface $buyer
     * @return string
     */
    public function shipOrder(AbstractOrder $order, BuyerInterface $buyer) : string {
        try {
            if(!$this->accessToken) throw new \Exception('Authorization error');

            $trackingNumber = "";
            if($this->createFulfillmentOrder($order, $buyer)){
                $createdOrder = $this->getFulfillmentOrder($order['order_unique']);
                $trackingNumber = $createdOrder['payload']
                    ['fulfillmentShipments'][0]
                    ['fulfillmentShipmentPackage'][0]
                    ['trackingNumber'] ?? "";
            }

            return $trackingNumber;
        } catch (\Throwable $th){
            Logger::log('Ship Order Error: '.$th->getMessage());
            throw new \RuntimeException($th->getMessage());
        }
    }

    /**
     * @param AbstractOrder $order
     * @param BuyerInterface $buyer
     * @return bool
     */
    private function createFulfillmentOrder(AbstractOrder $order, BuyerInterface $buyer){
        $endpoint = $this->host.'fba/outbound/2020-07-01/fulfillmentOrders';

        $options = [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorizations' => 'Bearer ' . $this->accessToken
            ],
            'json' => [
                'marketplaceId' => $this->marketplaceId,
                'sellerFulfillmentOrderId' => $order->data['order_unique'],
                'displayableOrderId' => $order->data['order_id'],
                'displayableOrderDate' => $order->data['order_date'],
                'displayableOrderComment' => $order->data['comments'],
                'shippingSpeedCategory' => 'Standard',
                'destinationAddress' => [
                    'name' => $buyer['shop_username'],
                    'addressLine1' => $buyer['address'],
                    'stateOrRegion' => $buyer['address'],
                    'postalCode' => $buyer['address'],
                    'countryCode' => $buyer['country_code']
                ],
                'items' => array_map(function ($product){
                    return [
                        'sellerSku' => $product['sku'],
                        'sellerFulfillmentOrderItemId' => $product['order_product_id'],
                        'quantity' => $product['ammount'],
                    ];
                }, $order['products'])
            ]
        ];

        $result = $this->call($endpoint, $options, 'POST');

        return $result['statusCode'] == 200;
    }

    /**
     * @param string $orderUniqueNumber
     * @return array
     */
    private function getFulfillmentOrder(string $orderUniqueNumber) : array {
        $endpoint = $this->host.'fba/outbound/2020-07-01/fulfillmentOrders/'.$orderUniqueNumber;

        $options = [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorizations' => 'Bearer ' . $this->accessToken
            ],
        ];

        $result = $this->call($endpoint, $options);
        return $result['data'] ?? [];
    }


}